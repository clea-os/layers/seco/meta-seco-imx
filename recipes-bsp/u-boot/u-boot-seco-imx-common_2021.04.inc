LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=5a7450c57ffe5ae63fd732446b988025"

UBOOT_SRC = "git://git.seco.com/clea-os/bsp/nxp/u-boot-seco-imx.git;"
PROTOCOL = "protocol=https;"
REPO_USER = ""
SRCBRANCH = "seco_lf_v2021.04"
SRC_URI = "${UBOOT_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"

SRCREV = "${AUTOREV}"

BPV = "2021.04"
PV = "${BPV}+gitr${SRCPV}"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

inherit fsl-u-boot-localversion

LOCALVERSION ?= "-${SRCBRANCH}"
