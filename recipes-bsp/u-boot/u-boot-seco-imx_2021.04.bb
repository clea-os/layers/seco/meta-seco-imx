DESCRIPTION = "i.MX U-Boot suppporting SECO boards"

require u-boot-seco-imx-common_${PV}.inc
require recipes-bsp/u-boot/u-boot.inc
inherit pythonnative

PROVIDES += "u-boot"
DEPENDS:append = " python-native dtc-native flex-native bison-native"

do_configure:append() {
    for config in ${UBOOT_MACHINE}; do
        sed -i 's/^CONFIG_DEFAULT_FDT_OVERLAY_CARRIER_FILE.*/CONFIG_DEFAULT_FDT_OVERLAY_CARRIER_FILE=\"${DEFAULT_DEVICETREE_OVERLAY_CARRIER}\"/g' ${B}/${config}/.config
    done
}

BOOT_TOOLS = "imx-boot-tools"

do_deploy:append:mx8m-nxp-bsp() {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    install -d ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/arch/arm/dts/${UBOOT_DTB_NAME} ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/u-boot-nodtb.bin ${DEPLOYDIR}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${type}
                    install -m 0777 ${B}/${config}/tools/mkimage ${DEPLOYDIR}/${BOOT_TOOLS}/mkimage_uboot
                fi
            done
            unset  j
        done
        unset  i
    fi
}


PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(mx6|mx8)"

UBOOT_NAME_mx6 = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
UBOOT_NAME_mx8 = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
