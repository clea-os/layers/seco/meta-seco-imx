DESCRIPTION = "i.MX U-Boot suppporting SECO Northern Europe Boards"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://Licenses/gpl-2.0.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"

UBOOT_SRC = "git://git.seco.com/seco-ne/3rd-party/kuk/uboot-imx-kuk.git;"
PROTOCOL = "protocol=https;"
REPO_USER = ""
SRCBRANCH = "guf_imx_v2020.04-trizeps8plus_2021_12_07;nobranch=1"
SRC_URI = "${UBOOT_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"

SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

# inherit fsl-u-boot-localversion

LOCALVERSION ?= "-${SRCBRANCH}"
