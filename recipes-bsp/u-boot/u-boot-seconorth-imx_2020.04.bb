#
# SPDX-License-Identifier: MIT
#

require u-boot-seconorth-imx-common_${PV}.inc
require recipes-bsp/u-boot/u-boot.inc

PROVIDES += "u-boot"
DEPENDS:append= " python-native dtc-native flex-native bison-native"

BOOT_TOOLS = "imx-boot-tools"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(mx8-generic-bsp)"

UBOOT_NAME:mx8mm-nxp-bsp = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"

FILES:${PN}-env += "${sysconfdir}/fw_env*.config"

do_compile:append:seco-imx8mm-tanaro() {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        [ -n "$CONFIG_ENV_OFFSET" ] || CONFIG_ENV_OFFSET=0x400000
        [ -n "$CONFIG_ENV_SIZE" ] || CONFIG_ENV_SIZE=0x2000

        echo "/dev/mmcblk0 $CONFIG_ENV_OFFSET $CONFIG_ENV_SIZE" > ${B}/fw_env-${MACHINE}.config
    fi
}

do_install:append:seco-imx8mm-tanaro() {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        install -D -m 0644 ${B}/fw_env-${MACHINE}.config ${D}/${sysconfdir}/fw_env-${MACHINE}.config
        ln -sf fw_env-${MACHINE}.config ${D}/${sysconfdir}/fw_env.config
    fi
}

# The DTB is required to build the boot binary (also see imx-boot and imx-mkimage)
do_deploy:append:mx8mm-nxp-bsp() {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    install -d ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/arch/arm/dts/${UBOOT_DTB_NAME} ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/u-boot-nodtb.bin ${DEPLOYDIR}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${type}
                fi
            done
            unset  j
        done
        unset  i
    fi

    install -m 0744 ${B}/${config}/tools/mkimage ${DEPLOYDIR}/${BOOT_TOOLS}/mkimage_uboot
}

