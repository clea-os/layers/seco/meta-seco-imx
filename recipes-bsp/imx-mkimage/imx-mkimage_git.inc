DEPENDS = "zlib-native openssl-native"

IMX_MKIMAGE_SRC ?= "git://git.seco.com/clea-os/bsp/nxp/tools/imx-mkimage.git;"
PROTOCOL ?= "protocol=https;"
REPO_USER ?= ""
SRCBRANCH = "seco_lf-5.10.52_2.1.0"
SRC_URI = "${IMX_MKIMAGE_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

BOOT_TOOLS = "imx-boot-tools"
SYSROOT_DIRS += "/boot"
