require imx-mkimage_git.inc

DESCRIPTION = "i.MX make image for SECO's boards"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
SECTION = "BSP"

inherit deploy native

PV = "1.0-git${SRCPV}"

CFLAGS = "-O2 -Wall -std=c99 -I ${STAGING_INCDIR} -L ${STAGING_LIBDIR}"

# The mkimage_imx8 is already located in our imx-mkimage repository.
# Therefore we don't build it again and also don't install it.

do_compile () {
    cd ${S}
    oe_runmake clean
    oe_runmake bin
#    oe_runmake -C iMX8M -f soc.mak mkimage_imx8
}

# The buildsystem is unable to parse an empty function (e.g. xy() {} ).
# Therefore, the install function needs to be disabled completely.

#do_install () {
#    cd ${S}
#    install -d ${D}${bindir}
#    install -m 0755 iMX8M/mkimage_imx8 ${D}${bindir}/mkimage_imx8m
#    install -m 0755 mkimage_imx8 ${D}${bindir}/mkimage_imx8
#}
