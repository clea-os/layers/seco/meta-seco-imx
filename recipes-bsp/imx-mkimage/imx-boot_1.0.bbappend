require imx-mkimage_git.inc

DEPENDS:remove = " imx-atf "

DEPENDS += "coreutils-native"

IMX_BL31:mx8-nxp-bsp = "bl31.bin"
IMX_BL31:mx8mn-nxp-bsp = "bl31-imx8mn.bin"
IMX_BL31:mx8mm-nxp-bsp = "bl31-imx8mm.bin"
IMX_BL31:mx8mp-nxp-bsp = "bl31-imx8mp.bin"
IMX_BL31:mx8mq-nxp-bsp = "bl31-imx8mq.bin"

# Some boards use a custom ATF firmware
IMX_BL31:seco-imx8mm-c61 = "bl31-imx8mm_c61.bin"
IMX_BL31:seco-imx8mm-tanaro = "bl31-imx8mm_tanaro.bin"
IMX_BL31:seco-imx8mm-myon2 = "bl31-imx8mm_myon2.bin"

do_compile[depends] = " \
    virtual/bootloader:do_deploy \
    ${@' '.join('%s:do_deploy' % r for r in '${IMX_EXTRA_FIRMWARE}'.split() )} \
    ${@bb.utils.contains('MACHINE_FEATURES', 'optee', 'optee-os:do_deploy', '', d)} \
"

compile_mx8() {
    bbnote 8QM boot binary build

    if [[ -L ${BOOT_STAGING}/scfw_tcm.bin ]]; then
        unlink ${BOOT_STAGING}/scfw_tcm.bin
    fi

    ln -s ${BOOT_STAGING}/${SC_FIRMWARE_NAME}   ${BOOT_STAGING}/scfw_tcm.bin
    cp ${DEPLOY_DIR_IMAGE}/${UBOOT_NAME}        ${BOOT_STAGING}/u-boot.bin

    if [ -e ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${UBOOT_CONFIG} ] ; then
        cp ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${UBOOT_CONFIG} \
                                                ${BOOT_STAGING}/u-boot-spl.bin
    fi
}

# Some of the artifacts for building the boot binary are already located at the
# BOOT_STAGING. We don't want to overwrite the existing files.
compile_mx8m() {
    bbnote 8MQ/8MM/8MN/8MP boot binary build
    cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/mkimage_uboot  ${BOOT_STAGING}

    cp ${DEPLOY_DIR_IMAGE}/signed_dp_imx8m.bin          ${BOOT_STAGING}
    cp ${DEPLOY_DIR_IMAGE}/signed_hdmi_imx8m.bin        ${BOOT_STAGING}
    cp ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${UBOOT_CONFIG} \
                                                        ${BOOT_STAGING}/u-boot-spl.bin
    cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/${UBOOT_DTB_NAME} \
                                                        ${BOOT_STAGING}
    cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${UBOOT_CONFIG} \
                                                        ${BOOT_STAGING}/u-boot-nodtb.bin
    cp ${BOOT_STAGING}/${IMX_BL31}                      ${BOOT_STAGING}/bl31.bin
    cp ${DEPLOY_DIR_IMAGE}/${UBOOT_NAME}                ${BOOT_STAGING}/u-boot.bin

    if [ -e ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/mkimage_uboot ]; then
        cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/mkimage_uboot ${BOOT_STAGING}/mkimage_uboot
    fi
}

compile_mx8x() {
    bbnote 8QX boot binary build
    
    # The files:
    # - ${SECO_FIRMWARE_NAME} is already in ${BOOT_STAGING}
    # - ${IMX_BL31} is already in ${BOOT_STAGING}
    # - ${SC_FIRMWARE_NAME} is already in ${BOOT_STAGING} and needs to be renamed
    # - ${UBOOT_NAME} must be moved into ${BOOT_STAGING} and renamed "u-boot-bin"

    cp ${BOOT_STAGING}/${SC_FIRMWARE_NAME}                 ${BOOT_STAGING}/scfw_tcm.bin   
    cp ${DEPLOY_DIR_IMAGE}/${UBOOT_NAME}                   ${BOOT_STAGING}/u-boot.bin
    if [ -e ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${UBOOT_CONFIG} ] ; then
        cp ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${UBOOT_CONFIG} \
                                                             ${BOOT_STAGING}/u-boot-spl.bin
    fi
}