# TODO: Decrease boot counter

echo "Loading artifacts ..."

echo "initrd_addr: ${initrd_addr}"
echo "fdt_addr: ${fdt_addr}"
echo "loadaddr: ${loadaddr}"

# Load RAM Filesystem
ramfs=-
if test -n ${ramfs_file}; then
    if fatload mmc 0:${ramfs_partition} ${initrd_addr} ${ramfs_file}; then
        ramfs=${initrd_addr}:${filesize}
        echo "ramfs: ${ramfs}"
    else
        echo "Error: Unable to load ${ramfs_file}"
    fi
else
    echo "Error: The ramfs name is not set"
fi

if ext4load mmc ${current_partition} ${loadaddr} boot/boot.cfg && env import -t ${loadaddr} ${filesize}; then

    # Set Kernel Parameters
    if test -n ${cmdline}; then
        echo "cmdline: ${cmdline}"
        setenv bootargs "${bootargs} ${cmdline}"
    fi

    # Load DT
    if test -n ${devicetree}; then
        echo "devicetree: ${devicetree}"

        if fatload mmc 0:${fdt_partition} ${fdt_addr} ${devicetree}; then
            true
        else
            echo "Error: Unable to load ${devicetree}"
        fi
    fi

    # Load Kernel
    echo "kernel: ${kernel}"
    if fatload mmc 0:${kernel_partition} ${loadaddr} ${kernel}; then
        booti ${loadaddr} ${ramfs} ${fdt_addr}
    else
        echo "Error: Unable to load ${kernel}"
    fi

else
    echo "Error: Unable to load boot.cfg"
fi
