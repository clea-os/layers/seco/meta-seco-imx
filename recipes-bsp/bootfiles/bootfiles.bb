#
# SPDX-License-Identifier: Apache
#

DESCRIPTION = "Bootfiles for SECO Northern Europe HMIs"
HOMEPAGE = "https://www.seco.com"
SECTION = "BSP"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS += "u-boot-mkimage-native"

RPROVIDES:${PN} += " boot.cfg boot.scr "
PROVIDES += " boot.cfg boot.scr "

SRC_URI = " \
    file://boot.cfg \
    file://boot.scr.sh \
"

do_configure() {
    sed -i ${WORKDIR}/boot.cfg -e "s|kernel=.*|kernel=Image|" \
        -e "s|devicetree=.*|devicetree=${DEFAULT_DEVICETREE}|"
}

do_compile() {
    mkimage -A arm -T script -O linux -d ${WORKDIR}/boot.scr.sh ${WORKDIR}/boot.scr
}

do_install() {
    install -d ${D}/boot
    install -m 0644 ${WORKDIR}/boot.cfg   ${D}/boot/boot.cfg
    install ${WORKDIR}/boot.scr           ${D}/boot/boot.scr
}

FILES:${PN} += "boot/*"
