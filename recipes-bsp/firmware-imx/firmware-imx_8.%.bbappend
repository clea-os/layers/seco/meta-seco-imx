FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# SOURCES
# dpfw.bin: package firmware-imx-8.11
SRC_URI += " \
    file://dpfw.bin \
"

do_install:append:mx8-generic-bsp() {
    install -m 0644 ${WORKDIR}/dpfw.bin ${D}${nonarch_base_libdir}/firmware
}
