
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " \
	file://0001-backend-drm-improve-atomic-commit-failure-handling.patch \
	file://0002-compositor-destroy-the-layout-after-the-compositor.patch \
	file://0003-backend-drm-make-sure-all-buffers-are-released-when-.patch \
	file://0004-kms-fix-hotplug-HDMI-meet-black-screen-when-play-vid.patch \
	file://0005-i.MX-backend-drm-kernel-dump-when-plugout-the-hdmi-c.patch \
	file://0006-i.MX-backend-drm-dmabuf-leak-during-hdmi-hotplug.patch \
"
