From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Gianfranco Mariotti <gianfranco.mariotti@seco.com>
Date: Tue, 29 Aug 2023 16:22:01 +0200
Subject: backend-drm: make sure all buffers are released when an output is
 removed

When an output is destroyed then the output state is freed immediately. In this
case, the plane state is only partially destroyed because it is the currently
active state. This includes the buffer reference.

Without the output, the plane will not be updated any more until it is used by a
different output (if possible) or the output returns and the plane is used
again.
As a result, the buffer reference is kept for a long time. This will cause some
applications to stall because weston now keeps two buffers (the one here and
another one for a different output where the application is now displayed).

To avoid this, do a synchronous commit that disables the output. The output
needs to be disabled anyways and this way the current state contains no
buffers that would remain.

`device->state_invalid = true` in drm_output_detach_crtc() is no longer
needed, because drm_output_detach_crtc() is called only when initialization
failed and the crtc was not yet used or in drm_output_deinit() when the
crtc was already disabled with the new synchronous commit.

REFERENCE: https://gitlab.freedesktop.org/wayland/weston/-/merge_requests/923
---
 libweston/backend-drm/drm.c | 11 +++++++----
 1 file changed, 7 insertions(+), 4 deletions(-)

diff --git a/libweston/backend-drm/drm.c b/libweston/backend-drm/drm.c
index 85933b95..ec783396 100644
--- a/libweston/backend-drm/drm.c
+++ b/libweston/backend-drm/drm.c
@@ -1844,14 +1844,10 @@ drm_output_attach_crtc(struct drm_output *output)
 static void
 drm_output_detach_crtc(struct drm_output *output)
 {
-	struct drm_backend *b = output->backend;
 	struct drm_crtc *crtc = output->crtc;
 
 	crtc->output = NULL;
 	output->crtc = NULL;
-
-	/* Force resetting unused CRTCs */
-	b->state_invalid = true;
 }
 
 static int
@@ -1925,6 +1921,13 @@ drm_output_deinit(struct weston_output *base)
 {
 	struct drm_output *output = to_drm_output(base);
 	struct drm_backend *b = to_drm_backend(base->compositor);
+	struct drm_pending_state *pending;
+
+	if (!b->shutting_down) {
+		pending = drm_pending_state_alloc(b);
+		drm_output_get_disable_state(pending, output);
+		drm_pending_state_apply_sync(pending);
+	}
 
 	if (b->use_pixman)
 		drm_output_fini_pixman(output);
-- 
2.25.1

