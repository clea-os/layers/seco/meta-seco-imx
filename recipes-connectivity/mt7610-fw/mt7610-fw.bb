DESCRIPTION = "MediaTek MT7610 firmware"

LICENSE = "LICENSE-mt7610-fw"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENCE.mediatek;md5=7c1976b63217d76ce47d0a11d8a79cf2"

NO_GENERIC_LICENSE[LICENSE-mt7610-fw] = "${WORKDIR}/LICENCE.mediatek"

SRC_URI = " \
    file://firmware-mt7610.tar.gz \
    file://LICENCE.mediatek \
"

do_install () {
    install -d ${D}/lib/firmware/mediatek
    install -m 0644 ${WORKDIR}/firmware-mt7610/mt7610e.bin ${D}/lib/firmware/mediatek/
    install -m 0644 ${WORKDIR}/firmware-mt7610/mt7610u.bin ${D}/lib/firmware/mediatek/
    # license
    install -m 0644 ${WORKDIR}/LICENCE.mediatek ${D}/lib/firmware/
}

FILES:${PN} += " \
    /lib/firmware/mediatek/* \
    /lib/firmware/LICENCE.mediatek \
"
