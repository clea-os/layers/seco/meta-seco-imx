DESCRIPTION = "TI WiLink 8 firmware"

LICENSE = "CLOSED"
RDEPENDS:${PN} += "linux-firmware-ti-connectivity-license"

# SOURCES
# https://git.ti.com/cgit/ti-bt/service-packs/tree/xml
# https://git.ti.com/cgit/wilink8-wlan/wl18xx_fw/tree/
# https://git.ti.com/cgit/wilink8-wlan/18xx-ti-utils/tree/wlconf
SRC_URI = " \
    file://TIInit_11.8.32.bts \
    file://wl18xx-fw-4.bin \
    file://wl127x-nvs.bin \
    file://wl18xx-conf.bin \
"

do_install () {
    install -d ${D}/lib/firmware/ti-connectivity
    install -m 0644 ${WORKDIR}/TIInit_11.8.32.bts ${D}/lib/firmware/ti-connectivity/
    install -m 0644 ${WORKDIR}/wl18xx-fw-4.bin ${D}/lib/firmware/ti-connectivity/
    install -m 0644 ${WORKDIR}/wl127x-nvs.bin ${D}/lib/firmware/ti-connectivity/
    install -m 0644 ${WORKDIR}/wl18xx-conf.bin ${D}/lib/firmware/ti-connectivity/
    # driver requirement: NVS filename
    install -m 0644 ${WORKDIR}/wl127x-nvs.bin ${D}/lib/firmware/ti-connectivity/wl1271-nvs.bin
}

FILES:${PN} += "/lib/firmware/ti-connectivity/*"
