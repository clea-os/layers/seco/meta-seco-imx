# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# Unable to find any files that looked like license statements. Check the accompanying
# documentation and source headers and set LICENSE and LIC_FILES_CHKSUM accordingly.
#
# NOTE: LICENSE is being set to "CLOSED" to allow you to at least start building - if
# this is not accurate with respect to the licensing of the software being built (it
# will not be in most cases) you must specify the correct value before using this
# recipe for anything other than initial testing/development!
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = " \
    git://github.com/Poco-Ye/rtl8723DS-BT-uart-driver.git;protocol=https;branch=master \
    file://0001-rkt_hciattach-Fix-Makefile-for-the-Yocto-build.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "793362d55797668cc0dcd242b095c5361838a4ae"

S = "${WORKDIR}/git"

do_configure() {
	cd rtk_hciattach
	oe_runmake clean
}

do_compile:prepend() {
	cd rtk_hciattach
}

do_install() {
	mkdir -p ${D}${bindir}
	install -m755 ${S}/rtk_hciattach/rtk_hciattach ${D}${bindir}/
	mkdir -p ${D}/lib/firmware/rtlbt/
	install -m755 ${S}/8723D/rtl8723d_config ${D}/lib/firmware/rtlbt/
	install -m755 ${S}/8723D/rtl8723d_fw ${D}/lib/firmware/rtlbt/
}

FILES:${PN} += " \
	/lib/firmware/rtlbt/rtl8723d_config \
	/lib/firmware/rtlbt/rtl8723d_fw \
"

