DESCRIPTION = "Murata Type 1MW firmware"

LICENSE = "CLOSED"
RDEPENDS:${PN} += "linux-firmware-broadcom-license"

# version 7.45.184 (r712131 CY)
SRC_URI = " \
    file://firmware-1mw.tar.gz \
"

do_install () {
    install -d ${D}/lib/firmware/brcm
    install -m 0644 ${WORKDIR}/firmware-1mw/brcmfmac43455-sdio.bin      ${D}/lib/firmware/brcm/
    install -m 0644 ${WORKDIR}/firmware-1mw/brcmfmac43455-sdio.clm_blob ${D}/lib/firmware/brcm/
    install -m 0644 ${WORKDIR}/firmware-1mw/brcmfmac43455-sdio.txt      ${D}/lib/firmware/brcm/
    # driver requirement: fw txt filename suffixed by the board dts compatible[0]
    install -m 0644 ${WORKDIR}/firmware-1mw/brcmfmac43455-sdio.txt      ${D}/lib/firmware/brcm/brcmfmac43455-sdio.fsl,imx8mm-evk.txt
    install -m 0644 ${WORKDIR}/firmware-1mw/brcmfmac43455-sdio.txt      ${D}/lib/firmware/brcm/brcmfmac43455-sdio.seco,imx8qm-c43.txt
}

FILES:${PN} += "/lib/firmware/brcm/*"
