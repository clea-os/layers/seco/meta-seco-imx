DESCRIPTION = "Edimax EW-7611ULB Realtek RTL8723BU firmware"

LICENSE = "CLOSED"
RDEPENDS:${PN} += "linux-firmware-rtl-license"

SRC_URI = " \
    file://firmware-ew-7611ulb.tar.gz \
"

do_install () {
    # WiFi fw
    install -d ${D}/lib/firmware/rtlwifi
    install -m 0644 ${WORKDIR}/firmware-ew-7611ulb/wifi/rtl8723bu_ap.bin     ${D}/lib/firmware/rtlwifi/
    install -m 0644 ${WORKDIR}/firmware-ew-7611ulb/wifi/rtl8723bu_nic.bin    ${D}/lib/firmware/rtlwifi/
    install -m 0644 ${WORKDIR}/firmware-ew-7611ulb/wifi/rtl8723bu_wowlan.bin ${D}/lib/firmware/rtlwifi/
    # BT fw
    install -d ${D}/lib/firmware/rtl_bt
    install -m 0644 ${WORKDIR}/firmware-ew-7611ulb/bt/rtl8723b_fw.bin     ${D}/lib/firmware/rtl_bt/
    install -m 0644 ${WORKDIR}/firmware-ew-7611ulb/bt/rtl8723b_config.bin ${D}/lib/firmware/rtl_bt/
}

FILES:${PN} += " \
    /lib/firmware/rtlwifi/* \
    /lib/firmware/rtl_bt/* \
"
