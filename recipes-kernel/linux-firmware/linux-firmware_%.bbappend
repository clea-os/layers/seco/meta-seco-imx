# Copyright 2017-2021 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://firmware-p162-sd8997.tgz \
"

do_install:append() {
    install -d ${D}${nonarch_base_libdir}/firmware/mrvl
    install -m 0644 ${WORKDIR}/firmware-sd8997/mrvl/cal_data.conf         ${D}${nonarch_base_libdir}/firmware/mrvl
    install -m 0644 ${WORKDIR}/firmware-sd8997/mrvl/sd8997_bt_v4.bin      ${D}${nonarch_base_libdir}/firmware/mrvl
    install -m 0644 ${WORKDIR}/firmware-sd8997/mrvl/sd8997_wlan_v4.bin    ${D}${nonarch_base_libdir}/firmware/mrvl
    install -m 0644 ${WORKDIR}/firmware-sd8997/mrvl/sdsd8997_combo_v4.bin ${D}${nonarch_base_libdir}/firmware/mrvl

    install -d ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/ed_mac_ctrl_V3_8987.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/ed_mac_ctrl_V3_8997.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/ed_mac_ctrl_V3_909x.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/pcieuart8997_combo_v4.bin  ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/pcieuart9098_combo_v1.bin  ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/sdiouart8987_combo_v0.bin  ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/sdiouart8997_combo_v4.bin  ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/sdiouartiw416_combo_v0.bin ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/txpwrlimit_cfg_8987.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/txpwrlimit_cfg_8997.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/txpwrlimit_cfg_9098.conf   ${D}${nonarch_base_libdir}/firmware/nxp
    install -m 0644 ${WORKDIR}/firmware-sd8997/nxp/wifi_mod_para.conf         ${D}${nonarch_base_libdir}/firmware/nxp

    # avoid file conflicts with recipes-connectivity
    rm -rf ${D}${nonarch_base_libdir}/firmware/brcm/brcmfmac43455-sdio.*
    rm -rf ${D}${nonarch_base_libdir}/firmware/cypress/cyfmac43455-sdio.*
    rm -rf ${D}${nonarch_base_libdir}/firmware/ti-connectivity/TI*
    rm -rf ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl127x-nvs.bin
    rm -rf ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl1271-nvs.bin
    rm -rf ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl12*
    rm -rf ${D}${nonarch_base_libdir}/firmware/ti-connectivity/wl18*
    rm -rf ${D}${nonarch_base_libdir}/firmware/rtlwifi/rtl8723bu*.bin
    rm -rf ${D}${nonarch_base_libdir}/firmware/rtl_bt/rtl8723b_fw.bin
}

FILES:${PN}-sd8997 += " \
    ${nonarch_base_libdir}/firmware/mrvl/cal_data.conf \
    ${nonarch_base_libdir}/firmware/mrvl/sd8997_bt_v4.bin \
    ${nonarch_base_libdir}/firmware/mrvl/sd8997_wlan_v4.bin \
    ${nonarch_base_libdir}/firmware/mrvl/sdsd8997_combo_v4.bin \
    ${nonarch_base_libdir}/firmware/nxp/ed_mac_ctrl_V3_8987.conf \
    ${nonarch_base_libdir}/firmware/nxp/ed_mac_ctrl_V3_8997.conf \
    ${nonarch_base_libdir}/firmware/nxp/ed_mac_ctrl_V3_909x.conf \
    ${nonarch_base_libdir}/firmware/nxp/pcieuart8997_combo_v4.bin \
    ${nonarch_base_libdir}/firmware/nxp/pcieuart9098_combo_v1.bin \
    ${nonarch_base_libdir}/firmware/nxp/sdiouart8987_combo_v0.bin \
    ${nonarch_base_libdir}/firmware/nxp/sdiouart8997_combo_v4.bin \
    ${nonarch_base_libdir}/firmware/nxp/sdiouartiw416_combo_v0.bin \
    ${nonarch_base_libdir}/firmware/nxp/txpwrlimit_cfg_8987.conf \
    ${nonarch_base_libdir}/firmware/nxp/txpwrlimit_cfg_8997.conf \
    ${nonarch_base_libdir}/firmware/nxp/txpwrlimit_cfg_9098.conf \
    ${nonarch_base_libdir}/firmware/nxp/wifi_mod_para.conf \
"

# avoid file conflicts with recipes-connectivity
FILES:${PN}-bcm43455 = ""
FILES:${PN}-wlcommon = ""
FILES:${PN}-wl12xx = ""
FILES:${PN}-wl18xx = ""
FILES:${PN}-rtl8723:remove = "${nonarch_base_libdir}/firmware/rtlwifi/rtl8723bu*.bin"
