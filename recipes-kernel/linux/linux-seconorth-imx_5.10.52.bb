#
# SPDX-License-Identifier: MIT
#

SUMMARY = "Linux Kernel for SECO NE"
SECTION = "kernel"

inherit kernel

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

DEPENDS += " openssl-native util-linux-native lzop-native "

EXTRA_OEMAKE += " 'DTC_FLAGS=-@' "

# Remove ${PKGE}- from file names, as PE is always unset,
# it generates the -- in the name
KERNEL_ARTIFACT_NAME = "${PKGV}-${PKGR}-${MACHINE}${IMAGE_VERSION_SUFFIX}"

do_install:append() {
    # Remove the links, we have a fat partition as /boot
    find "${D}/${KERNEL_IMAGEDEST}/" -type l -exec rm -v {} \;
}

# i.MX8M

KERNEL_CONFIG_COMMAND = "oe_runmake_call -C ${S} CC="${KERNEL_CC}" O=${B} olddefconfig"

COMPATIBLE_MACHINE = "(seco-imx8mm-tanaro)"

LINUX_VERSION = "5.10"
LINUX_VERSION_EXTENSION = "-seconorth"

FILESEXTRAPATHS:prepend := "${THISDIR}/linux-seconorth-${LINUX_VERSION}:"

S = "${WORKDIR}/git"

PV = "5.10.52"
SRCREV = "${AUTOREV}"

# branch= combined with nobranch=1 looks weird, but the branch is used for the
# AUTOREV, the nobranch tells the fetcher to not check the SHA validation for the branch
# This way the SRCREV can be specified to a specific revision also outside that branch
# with SRCREV_pn-linux-guf =
SRC_URI = " \
    git://git.seco.com/seco-ne/kernel/linux-imx-kuk.git;protocol=ssh;branch=linux-${PV}-seco;nobranch=1 \
    file://defconfig \
"

