SUMMARY = "Linux Kernel provided and supported by SECO"
DESCRIPTION = "Linux Kernel provided and supported by SECO with focus on \
SECO Boards. It includes support for many IPs such as GPU, VPU and IPU."

include linux-seco-imx-src-5.10.52.inc

inherit kernel-yocto kernel

# Put a local version until we have a true SRCREV to point to
LOCALVERSION ?= ""
SCMVERSION ?= "y"
SRCBRANCH ?= ""


DEPENDS += "lzop-native bc-native"

COMPATIBLE_MACHINE = "(mx6|mx7|mx8)"
EXTRA_OEMAKE:append:mx6-nxp-bsp = " ARCH=arm"
EXTRA_OEMAKE:append:mx8-nxp-bsp = " ARCH=arm64"

#KBUILD_DEFCONFIG     = "defconfig"
KBUILD_DEFCONFIG:mx6-nxp-bsp ?= "seco_imx6_linux_defconfig"
KBUILD_DEFCONFIG:mx8-nxp-bsp ?= "seco_imx8_linux_defconfig"

KCONFIG_MODE="--alldefconfig"

S = "${WORKDIR}/git"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:seco-c72 = " file://disable-old-wifi-driver.cfg"
SRC_URI:append:seco-imx8qm-c43 = " file://disable-old-wifi-driver.cfg file://usbwifi.cfg"

SRC_URI:append:mx8-nxp-bsp = " file://rfkill.cfg"

do_preconfigure:prepend() {
    install -d ${B}
    cp ${S}/arch/${ARCH}/configs/${KBUILD_DEFCONFIG} ${WORKDIR}/defconfig
}

# set kernel firmware blobs string
KBUILD_FIRMWARE:append = "regulatory.db regulatory.db.p7s "
KBUILD_FIRMWARE:append:mx6-nxp-bsp = "imx/sdma/sdma-imx6q.bin "
KBUILD_FIRMWARE:append:mx8-nxp-bsp = "imx/sdma/sdma-imx7d.bin "
KBUILD_FIRMWARE:append:seco-imx8mm-c61 = "brcm/brcmfmac43455-sdio.bin brcm/brcmfmac43455-sdio.clm_blob brcm/brcmfmac43455-sdio.txt brcm/brcmfmac43455-sdio.fsl,imx8mm-evk.txt"
KBUILD_FIRMWARE:append:seco-imx8mq-c12 = "ti-connectivity/TIInit_11.8.32.bts ti-connectivity/wl18xx-fw-4.bin ti-connectivity/wl127x-nvs.bin ti-connectivity/wl18xx-conf.bin ti-connectivity/wl1271-nvs.bin"
KBUILD_FIRMWARE:append:seco-imx8qm-c43 = "brcm/brcmfmac43455-sdio.bin brcm/brcmfmac43455-sdio.clm_blob brcm/brcmfmac43455-sdio.txt brcm/brcmfmac43455-sdio.seco,imx8qm-c43.txt"

# fetch firmware blobs into staging directory
DEPENDS += " \
    wireless-regdb \
    linux-firmware \
    murata1mw-fw \
    wilink8-fw \
"

do_configure:append() {
    if [ -n "${KBUILD_FIRMWARE}" ]; then
        # set firmware blobs into kernel config
        echo "CONFIG_FW_LOADER=y" >> ${B}/.config
        echo "CONFIG_EXTRA_FIRMWARE=\"${KBUILD_FIRMWARE}\"" >> ${B}/.config
        echo "CONFIG_EXTRA_FIRMWARE_DIR=\"firmware\"" >> ${B}/.config

        # set firmware blobs into kernel sources
        mkdir -p ${S}/firmware && cp -av ${STAGING_DIR_HOST}/lib/firmware/regulatory.db* ${S}/firmware/
        mkdir -p ${S}/firmware/imx && cp -arv ${STAGING_DIR_HOST}/lib/firmware/imx ${S}/firmware/
        mkdir -p ${S}/firmware/brcm && cp -arv ${STAGING_DIR_HOST}/lib/firmware/brcm ${S}/firmware/
        mkdir -p ${S}/firmware/ti-connectivity && cp -arv ${STAGING_DIR_HOST}/lib/firmware/ti-connectivity ${S}/firmware/
    fi
}
