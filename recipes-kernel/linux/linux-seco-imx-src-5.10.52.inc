# Released under the MIT license (see COPYING.MIT for the terms)

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


KERNEL_SRC ?= "git://git.seco.com/clea-os/bsp/nxp/linux-seco-imx.git;"
PROTOCOL ?= "protocol=https;"
REPO_USER ?= ""
SRCBRANCH = "seco_lf-5.10.y"
SRC_URI = "${KERNEL_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"
SRCREV = "${AUTOREV}"

LINUX_VERSION ?= "5.10.52"
