#@TYPE: Machine
#@NAME: SECO i.MX8M Mini / Nano / Nano Lite uQ7 Module
#@SOC: i.MX8MN
#@DESCRIPTION: Machine configuration for SECO BOARDS
#@MAINTAINER: Yuri Mazzuoli <yuri.mazzuoli@seco.com>

require conf/machine/include/seco-imx8-common.inc
require conf/machine/include/arm/armv8a/tune-cortexa53.inc

MACHINE_FEATURES += " pci wifi bluetooth optee bcm43455 bcm4356 bcm4359"

MACHINEOVERRIDES =. "seco-c72:"

KERNEL_DEVICETREE_OVERLAYS ?= " \
   seco/overlays/seco-imx8m-c72-edp.dtbo \
   seco/overlays/seco-imx8m-c72-lvds-dual-215.dtbo \
   seco/overlays/seco-imx8m-c72-lvds-dual-156.dtbo \
   seco/overlays/seco-imx8m-c72-lvds-wvga.dtbo \
"

KERNEL_DEVICETREE:mx8mn-nxp-bsp:append = " seco/seco-imx8mn-c72.dtb "
KERNEL_DEVICETREE:mx8mm-nxp-bsp:append = " seco/seco-imx8mm-c72.dtb "

KERNEL_DEVICETREE:append = "\
   ${KERNEL_DEVICETREE_OVERLAYS} \
"

MACHINE_EXTRA_RRECOMMENDS:append = " \
   kernel-module-nxp89xx \
"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
   linux-firmware-sd8997 \
"

SERIAL_CONSOLES = "115200;ttymxc1"

LOADADDR = ""
UBOOT_SUFFIX = "bin"
UBOOT_MAKE_TARGET = ""
IMX_BOOT_SEEK = "32"

UBOOT_DTB_NAME = "seco-imx8mn-c72.dtb"
SPL_BINARY = "spl/u-boot-spl.bin"

IMX_BOOT_SOC_TARGET = "iMX8MN"
IMXBOOT_TARGETS = " \
   ${@bb.utils.contains('UBOOT_CONFIG', 'fspi', 'flash_ddr4_evk_flexspi', 'flash_ddr4_evk', d)}"

UBOOT_CONFIG ??= "sd"
UBOOT_CONFIG[sd] = "seco_imx8mn_c72_defconfig,sdcard"

IMAGE_BOOTLOADER = "imx-boot"
BOOT_SPACE = "65536"


KERNEL_MODULE_AUTOLOAD  += "mlan moal"
KERNEL_MODULE_PROBECONF += "moal"
