# Provides the i.MX8 SECO's boards common settings

require conf/machine/include/seco-base.inc

MACHINEOVERRIDES =. "seco-mx8:"

SERIAL_CONSOLES = "115200;ttymxc0"

#UBOOT_MAKE_TARGET = "spl_imx"

KERNEL_FEATURES:remove = " cfg/virtio.scc"

MACHINE_EXTRA_RDEPENDS += " kernel-modules"

MACHINE_HAS_VIVANTE_KERNEL_DRIVER_SUPPORT = "1"
