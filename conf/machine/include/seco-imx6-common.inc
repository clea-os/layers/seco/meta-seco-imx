# Provides the i.MX6 SECO's boards common settings

require conf/machine/include/seco-base.inc
require conf/machine/include/arm/armv7a/tune-cortexa9.inc

MACHINEOVERRIDES =. "seco-mx6:"

SERIAL_CONSOLES = "115200;ttymxc0"

UBOOT_MAKE_TARGET = "spl_imx"

UBOOT_SUFFIX = "imx"

KERNEL_FEATURES:remove = " cfg/virtio.scc"

MACHINE_EXTRA_RDEPENDS += " kernel-modules"

MACHINE_HAS_VIVANTE_KERNEL_DRIVER_SUPPORT = "1"
