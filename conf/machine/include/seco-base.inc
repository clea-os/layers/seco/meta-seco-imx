require conf/machine/include/imx-base.inc

# Override settings in imx-base.inc file

MACHINEOVERRIDES =. "seco-arm:"

SECO_IMX_DEFAULT_BOOTLOADER = "u-boot-seco-imx"
SECO_IMX_DEFAULT_BOOTLOADER:use-nxp-mx8 = "u-boot-seco-imx"

SECO_IMX_DEFAULT_KERNEL = "linux-seco-imx"

PREFERRED_PROVIDER_u-boot = "${SECO_IMX_DEFAULT_BOOTLOADER}"
PREFERRED_PROVIDER_fw-sysdata = "${SECO_IMX_DEFAULT_BOOTLOADER}-fw-sysdata"

IMX_DEFAULT_BSP = "nxp"

IMX_DEFAULT_KERNEL = "${SECO_IMX_DEFAULT_KERNEL}"
IMX_DEFAULT_KERNEL:mx6-nxp-bsp = "${SECO_IMX_DEFAULT_KERNEL}"
IMX_DEFAULT_KERNEL:mx8-nxp-bsp = "${SECO_IMX_DEFAULT_KERNEL}"

PREFERRED_PROVIDER_virtual/bootloader = "${SECO_IMX_DEFAULT_BOOTLOADER}"
PREFERRED_PROVIDER_virtual/kernel = "${SECO_IMX_DEFAULT_KERNEL}"

PREFERRED_VERSION_linux-seco-imx = "5.10%"
PREFERRED_VERSION_u-boot-seco-imx = "2021.04%"
PREFERRED_VERSION_fw-sysdata = "2021.04%"

PREFERRED_VERSION_gstreamer1.0-plugins-base = "1.20.3.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad  = "1.20.3.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good = "1.20.3.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good-qt = "1.20.3.imx"
PREFERRED_VERSION_gstreamer1.0              = "1.20.3.imx"

MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6dl-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6q-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6sl-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6sll-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6sx-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6ul-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6ull-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx6ulz-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx7d-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx7ulp-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx8-nxp-bsp = " imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:append:mx9-nxp-bsp = " imx-gst1.0-plugin"


MACHINE_GRAPHICS_TESTING_PLUGIN = ""
MACHINE_GRAPHICS_TESTING_PLUGIN:imx-nxp-bsp = "imx-test"
MACHINE_GRAPHICS_TESTING_PLUGIN:imxgpu  = "imx-test imx-gpu-viv-tools imx-gpu-viv-demos"
MACHINE_GRAPHICS_TESTING_PLUGIN:qoriq = "ceetm optee-test-qoriq"


MACHINE_HW_GRAPHICS_PLUGIN = ""
MACHINE_HW_GRAPHICS_PLUGIN:seco-mx6 = " \
    packagegroup-fsl-tools-gpu \
    packagegroup-fsl-tools-gpu-external \
"
MACHINE_HW_GRAPHICS_PLUGIN:seco-mx8 = " \
    packagegroup-fsl-tools-gpu \
    packagegroup-fsl-tools-gpu-external \
"

G2D_SAMPLES              = ""
G2D_SAMPLES:imxgpu2d     = "imx-g2d-samples"
G2D_SAMPLES:mx93-nxp-bsp = "imx-g2d-samples"


DEFAULT_DEVICETREE_OVERLAY_CARRIER ?= ""
